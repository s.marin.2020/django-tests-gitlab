from django.test import TestCase
from django.test import Client

from .views import Counter


class CounterTests(TestCase):

    def test_creation(self):
        """Compruebo la creación"""
        counter = Counter()
        self.assertEquals(counter.counter, 0)

    def test_increment(self):
        """Compurebo que se incremente"""
        counter = Counter()
        incremented = counter.increment()
        self.assertEquals(incremented, 1)


class PageTest(TestCase):

    def test_index(self):
        """Cmpruebo el recurso barra"""
        c = Client()
        response = c.get("/")
        self.assertEquals(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<hr>Pages viewed up to now:', content)

    def test_post(self):
        """Compruebo que se creen bien las entradas"""
        c = Client()
        item = "Hola a todos, esto es para el test"
        response = c.post('/hola', {'content': item})
        self.assertEquals(response.status_code, 200)
        response = c.get('/hola')
        self.assertEquals(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn(item, content)
